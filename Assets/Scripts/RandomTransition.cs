﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WindowedUI.Core;
using WindowedUI.Core.Extensions;

public class RandomTransition : WindowExtension
{
    public List<Transition> Transitions;

    protected override void OnBeforeWindowOpened()
    {
        int randomIndex = Random.Range(0, Transitions.Count);
        Transitions[randomIndex].Play(ParentWindow);
    }

    protected override void OnBeforeWindowClosed()
    {
        int randomIndex = Random.Range(0, Transitions.Count);
        Transitions[randomIndex].Play(ParentWindow);
    }
}
