using System;
using UnityEngine;
using UnityEngine.UI;

namespace WindowedUI.Tabs
{
    public class TabButton : MonoBehaviour
    {
        [SerializeField] private Button button;

        private Action<TabButton> _selectAction;

        public void Init(Action<TabButton> selectAction)
        {
            _selectAction = selectAction;

            button.onClick.AddListener(SelectTab);
        }

        private void SelectTab()
        {
            if (_selectAction == null)
            {
                Debug.LogError($"Tab selection callback is null");
                return;
            }

            _selectAction.Invoke(this);
        }

        private void OnValidate()
        {
            if (button == null)
                button = GetComponent<Button>();
        }
    }
}