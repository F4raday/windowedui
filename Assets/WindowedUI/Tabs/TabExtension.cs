using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WindowedUI.Core;
using WindowedUI.Core.Extensions;

namespace WindowedUI.Tabs
{
    public class TabExtension<T> : WindowExtension
    {
        [SerializeField] private List<TabEntry<T>> tabs;

        public TabEntry<T> ActiveTab { get; protected set; }

        protected internal override void OnBeforeInit()
        {
            foreach (var entry in tabs)
            {
                entry.Button.Init(SelectTab);
                entry.Panel.gameObject.SetActive(false);
            }
        }

        protected internal override void OnBeforeWindowOpened()
        {
            if (ActiveTab == null)
                SelectTab(0);
        }


        public void SelectTab(int tabIndex)
        {
            if (tabIndex < 0 || tabIndex >= tabs.Count)
            {
                Debug.LogError($"Can't open tab with index {tabIndex}. Expected value between {0} and {tabs.Count - 1}");
                return;
            }

            SelectTab(tabs[tabIndex]);
        }

        public void SelectTab(T tabKey)
        {
            SelectTab(tabs.FirstOrDefault(entry => entry.Key.Equals(tabKey)));
        }

        public void SelectTab(TabButton button)
        {
            SelectTab(tabs.FirstOrDefault(entry => entry.Button == button));
        }

        private void SelectTab(TabEntry<T> tabEntry)
        {
            if (tabEntry == null)
            {
                Debug.LogError($"Trying to open NULL tab");
                return;
            }

            if (ActiveTab != null)
            {
                ActiveTab.Panel.Close();
                ActiveTab.Panel.gameObject.SetActive(false);
            }

            ActiveTab = tabEntry;
            ActiveTab.Panel.gameObject.SetActive(true);
            ActiveTab.Panel.Open();
        }
    }

    [Serializable]
    public class TabEntry<T>
    {
        public TabButton Button;
        public TabPanel Panel;
        public T Key;
    }
}
