using System.Collections.Generic;
using UnityEngine;
using WindowedUI.Core;

namespace WindowedUI.Tabs
{
    public class TabPanel : MonoBehaviour
    {
        [SerializeField] private List<WindowView> views = new();

        public List<WindowView> Views => views;

        public void Open()
        {
            views.ForEach(view => view.Open());
        }

        public void Close()
        {
            views.ForEach(view => view.Close());
        }
    }

#if UNITY_EDITOR
    [UnityEditor.CustomEditor(typeof(TabPanel))]
    public class TabPanelEditor : UnityEditor.Editor
    {
        TabPanel _panel;

        private void OnEnable()
        {
            _panel = target as TabPanel;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (GUILayout.Button("Collect views"))
            {
                _panel.Views.Clear();
                _panel.Views.AddRange(_panel.GetComponentsInChildren<WindowView>());
                UnityEditor.EditorUtility.SetDirty(_panel);
            }
        }
    }
#endif 
}