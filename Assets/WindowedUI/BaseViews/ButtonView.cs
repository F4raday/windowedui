﻿using UnityEngine;
using UnityEngine.UI;
using WindowedUI.Core;

namespace WindowedUI.BaseViews
{
    public abstract class ButtonView : WindowView
    {
        [SerializeField] private Button _attachedButton;

        protected override void OnInit()
        {
            _attachedButton.onClick.AddListener(OnButtonPress);
        }

        protected abstract void OnButtonPress();

#if UNITY_EDITOR
        private void OnValidate()
        {
            if (_attachedButton == null)
                _attachedButton = GetComponent<Button>();

            if(_attachedButton == null)
                Debug.LogError($"Button not found on the object", this);
        }
#endif
    }
}
