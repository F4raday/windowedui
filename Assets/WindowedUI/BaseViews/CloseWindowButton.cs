﻿namespace WindowedUI.BaseViews
{
    public class CloseWindowButton : ButtonView
    {
        protected override void OnButtonPress()
        {
            ParentWindow.Close();
        }
    }
}
