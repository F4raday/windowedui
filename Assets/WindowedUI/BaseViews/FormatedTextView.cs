﻿using UnityEngine;

namespace WindowedUI.BaseViews
{
    public abstract class FormatedTextView : TextView
    {
        [SerializeField] private string prefix;
        [SerializeField] private string suffix;

        protected override void SetText(string text)
        {
            base.SetText($"{prefix}text{suffix}");
        }
    }
}
