﻿using System;
using UnityEngine;
using WindowedUI.Core;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace WindowedUI.BaseViews
{
    public enum WindowReferenceMethod { ByName, ByReference}
    public class OpenWindowButton : ButtonView
    {
        [SerializeField] private OpenMethod _openMethod = OpenMethod.OpenOnTop;
        [SerializeField] private WindowReferenceMethod _referenceMethod;
        [SerializeField, HideInInspector] private string _windowName;
        [SerializeField, HideInInspector] private UIWindow _windowReference;
        

        protected override void OnButtonPress()
        {
            switch (_referenceMethod)
            {
                case WindowReferenceMethod.ByName:
                    ParentWindow.Root.OpenWindow(_windowName, _openMethod);
                    break;
                case WindowReferenceMethod.ByReference:
                    ParentWindow.Root.OpenWindow(_windowReference, _openMethod);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

#if UNITY_EDITOR
    
    [CustomEditor(typeof(OpenWindowButton))]
    public class OpenWindowButtonEditor : Editor
    {
        SerializedProperty referenceMethod;
        SerializedProperty windowName;
        SerializedProperty windowReference;

        private void OnEnable()
        {
            referenceMethod = serializedObject.FindProperty("_referenceMethod");
            windowName = serializedObject.FindProperty("_windowName");
            windowReference = serializedObject.FindProperty("_windowReference");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            DrawDefaultInspector();

            if (referenceMethod.enumValueIndex == (int) WindowReferenceMethod.ByName)
            {
                EditorGUILayout.PropertyField(windowName);
            }
            else
            {
                EditorGUILayout.PropertyField(windowReference);
            }

            serializedObject.ApplyModifiedProperties();
        }
    }

#endif
}
