﻿using TMPro;
using UnityEngine;
using WindowedUI.Core;

namespace WindowedUI.BaseViews
{
    public abstract class TextView : WindowView
    {
        [SerializeField] protected TextMeshProUGUI _attachedText;

        protected virtual void SetText(string text)
        {
            _attachedText.SetText(text);
        }

        protected void SetText(int text)
        {
            SetText(text.ToString());
        }

        protected void SetText(float text)
        {
            SetText(text.ToString());
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            if (_attachedText == null)
                _attachedText = GetComponent<TextMeshProUGUI>();

            if (_attachedText == null)
                Debug.LogError($"Text not found on the object", this);
        }
#endif
    }
}
