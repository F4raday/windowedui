﻿using System;
using System.Collections.Generic;
using UnityEngine;
using WindowedUI.Core.Extensions;

namespace WindowedUI.BaseExtensions
{
    public class WindowChildrenDataExtension : WindowExtension
    {
        private Dictionary<string, RectTransform> _childObjects;

        protected internal override void OnBeforeInit()
        {
            _childObjects = new Dictionary<string, RectTransform>();
            _childObjects.Add(name, GetComponent<RectTransform>());
            RegisterChildObjects(_childObjects[name]);
        }

        public RectTransform GetObject(string objectName)
        {
            if (string.IsNullOrEmpty(objectName))
                return _childObjects[name];

            if (_childObjects.TryGetValue(objectName, out RectTransform value))
                return value;

            Debug.LogError($"Requesting object with name {objectName} which is not found");

            return null;
        }

        private void RegisterChildObjects(RectTransform obj)
        {
            foreach (RectTransform child in obj.transform)
            {
                if (!_childObjects.ContainsKey(child.name))
                    _childObjects.Add(child.name, child);

                RegisterChildObjects(child);
            }
        }

#if UNITY_EDITOR

        private void Reset()
        {
            _extensionUsageMethod = ExtensionUsageMethod.BeforeInit;
        }

#endif
    }
}
