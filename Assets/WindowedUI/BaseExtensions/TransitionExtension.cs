﻿using System;
using UnityEngine;
using WindowedUI.Core;
using WindowedUI.Core.Extensions;

namespace WindowedUI.BaseExtensions
{
    public class TransitionExtension : WindowExtension
    {
        [SerializeField] private Transition openTransition;
        [SerializeField] private Transition closeTransition;

        protected internal override void OnBeforeWindowOpened()
        {
            openTransition.Play(ParentWindow);
        }

        protected internal override void OnBeforeWindowClosed()
        {
            closeTransition.Play(ParentWindow);
        }

        private void Reset()
        {
            _extensionUsageMethod = ExtensionUsageMethod.BeforeOpen | ExtensionUsageMethod.BeforeClose;
        }
    }
}
