﻿using UnityEngine;
using WindowedUI.Core;

namespace WindowedUI.BaseTransitions.DefaultValues
{
    [System.Serializable]
    public abstract class ADefaultValue
    {
        public string ObjectName;

        public virtual void Set(UIWindow window, Transition transition)
        {
            Set(transition.GetObject(ObjectName), window, transition);
        }

        public virtual void Set(RectTransform objectTransform, UIWindow window, Transition transition)
        {

        }

        public ADefaultValue Clone() => (ADefaultValue)MemberwiseClone();

#if UNITY_EDITOR
        internal void OnGUI()
        {
            DrawGUI();
        }

        protected abstract void DrawGUI();
#endif
    }
}
