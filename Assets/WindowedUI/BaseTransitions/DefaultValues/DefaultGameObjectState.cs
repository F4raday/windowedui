﻿using UnityEditor;
using UnityEngine;
using WindowedUI.Core;

namespace WindowedUI.BaseTransitions.DefaultValues
{
    [System.Serializable]
    public class DefaultGameObjectState : ADefaultValue
    {
        public bool NewState;

        public override void Set(RectTransform objectTransform, UIWindow window, Transition transition)
        {
            objectTransform.gameObject.SetActive(NewState);
        }

#if UNITY_EDITOR
        protected override void DrawGUI()
        {
            ObjectName = EditorGUILayout.DelayedTextField("Object name", ObjectName);

            NewState = EditorGUILayout.Toggle("Is active", NewState);

            if (string.IsNullOrEmpty(ObjectName))
                EditorGUILayout.HelpBox("Should not be used on main object", MessageType.Warning);
        }
#endif
    }
}
