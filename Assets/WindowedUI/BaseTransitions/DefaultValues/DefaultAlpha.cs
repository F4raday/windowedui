﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using WindowedUI.Core;

namespace WindowedUI.BaseTransitions.DefaultValues
{
    [System.Serializable]
    public class DefaultAlpha : ADefaultValue
    {
        public bool IsImage;
        public float Value;

        public override void Set(RectTransform objectTransform, UIWindow window, Transition transition)
        {
            if (IsImage)
            {
                Image image = objectTransform.GetComponent<Image>();
                Color color = image.color;
                color.a = Value;
                image.color = color;
            }
            else
            {
                objectTransform.GetComponent<CanvasGroup>().alpha = Value;
            }
        }

#if UNITY_EDITOR
        protected override void DrawGUI()
        {
            ObjectName = EditorGUILayout.DelayedTextField("Object name", ObjectName);

            IsImage = EditorGUILayout.Toggle("Is image (not CanvasGroup)", IsImage);
            Value = EditorGUILayout.FloatField("Value", Value);
        }
#endif
    }
}
