﻿using UnityEditor;
using UnityEngine;
using WindowedUI.Core;

namespace WindowedUI.BaseTransitions.DefaultValues
{
    [System.Serializable]
    public class DefaultPosition : ADefaultValue
    {
        public bool IsWorldPosition;
        public Vector3 Position;

        public override void Set(RectTransform objectTransform, UIWindow window, Transition transition)
        {
            if (IsWorldPosition)
            {
                objectTransform.position = Position;
            }
            else
            {
                objectTransform.anchoredPosition = Position;
            }
        }

#if UNITY_EDITOR
        protected override void DrawGUI()
        {
            ObjectName = EditorGUILayout.DelayedTextField("Object name", ObjectName);

            Position = EditorGUILayout.Vector3Field("Position", Position);
            IsWorldPosition = EditorGUILayout.Toggle("Is world position", IsWorldPosition);
        }
#endif
    }
}
