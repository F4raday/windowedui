﻿using UnityEditor;
using UnityEngine;
using WindowedUI.Core;

namespace WindowedUI.BaseTransitions.DefaultValues
{
    public class DefaultScale : ADefaultValue
    {
        public Vector3 Scale;

        public override void Set(RectTransform objectTransform, UIWindow window, Transition transition)
        {
            objectTransform.localScale = Scale;
        }
#if UNITY_EDITOR
        protected override void DrawGUI()
        {
            ObjectName = EditorGUILayout.DelayedTextField("Object name", ObjectName);

            Scale = EditorGUILayout.Vector3Field("Scale", Scale);
        }
#endif
    }
}
