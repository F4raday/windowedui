﻿using UnityEditor;
using UnityEngine;
using WindowedUI.Core;

namespace WindowedUI.BaseTransitions.DefaultValues
{
    [System.Serializable]
    public class DefaultWindowStateBlocker : ADefaultValue
    {
        public bool State;

        public override void Set(RectTransform objectTransform, UIWindow window, Transition transition)
        {
            if (State)
                window.OpenBlockers.Add(transition);
            else
                window.CloseBlockers.Add(transition);
        }

        public static void SetState(UIWindow window, Transition transition, bool isOpening)
        {
            if (isOpening)
                window.OpenBlockers.Add(transition);
            else
                window.CloseBlockers.Add(transition);
        }

#if UNITY_EDITOR
        protected override void DrawGUI()
        {
            State = EditorGUILayout.Toggle("Is opening", State);

            if (string.IsNullOrEmpty(ObjectName))
                EditorGUILayout.HelpBox("Should not be used on main object if Transition type is not Custom", MessageType.Warning);
        }
#endif
    }
}