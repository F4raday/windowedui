﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEditor;
using UnityEngine;
using WindowedUI.BaseExtensions;
using WindowedUI.BaseTransitions.DefaultValues;
using WindowedUI.Core;

namespace WindowedUI.BaseTransitions
{
    [CreateAssetMenu(fileName = "DoTweenTransition", menuName = "Windowed UI/DoTween Transition")]
    public class DoTweenTransition : Transition
    {
        [SerializeField] public TransitionType TransitionType;
        [SerializeField] public float TransitionDuration;
        [SerializeReference] public List<ADefaultValue> _defaultValues = new List<ADefaultValue>();
        [SerializeReference] public List<ATweenElement> _tweenElements = new List<ATweenElement>();

        public override float Duration => totalDuration;

        private WindowChildrenDataExtension childrenData;
        private float totalDuration;

        public override void Play(UIWindow window)
        {
            childrenData = window.GetExtension<WindowChildrenDataExtension>();

            switch (TransitionType)
            {
                case TransitionType.Open:
                    DefaultWindowStateBlocker.SetState(window, this, true);
                    break;
                case TransitionType.Close:
                    DefaultWindowStateBlocker.SetState(window, this, false);
                    break;
            }

            foreach (ADefaultValue aDefaultValue in _defaultValues)
            {
                aDefaultValue.Set(window, this);
            }

            Sequence sequence = DOTween.Sequence();
            foreach (ATweenElement aTweenElement in _tweenElements)
            {
                aTweenElement.Play(window, this, sequence);
            }

            switch (TransitionType)
            {
                case TransitionType.Open:
                    WindowStateBlockerTween.SetState(window, this, sequence, true, TransitionDuration);
                    break;
                case TransitionType.Close:
                    WindowStateBlockerTween.SetState(window, this, sequence, false, TransitionDuration);
                    break;
            }

            totalDuration = Mathf.Max(_tweenElements.Select(element => element.StartTime + Duration).ToArray());
            totalDuration = Math.Max(totalDuration, TransitionDuration);
        }

        public override RectTransform GetObject(string objectName)
        {
            return childrenData.GetObject(objectName);
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(DoTweenTransition))]
    public class DoTweenTransitionEditor : Editor
    {
        private Type[] defaultValueTypes;
        private int selectedDefaultValueIndex;

        private Type[] tweenTypes;
        private int selectedTweenIndex;

        DoTweenTransition instance;

        private static GUILayoutOption miniButtonWidth = GUILayout.Width(20f);

        private void OnEnable()
        {
            tweenTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(assembly => assembly.GetTypes())
                .Where(p => typeof(ATweenElement).IsAssignableFrom(p) && !p.IsAbstract).ToArray();

            defaultValueTypes = AppDomain.CurrentDomain.GetAssemblies().SelectMany(assembly => assembly.GetTypes())
                .Where(p => typeof(ADefaultValue).IsAssignableFrom(p) && !p.IsAbstract).ToArray();

            instance = target as DoTweenTransition;
        }

        public override void OnInspectorGUI()
        {
            // Header
            instance.TransitionType = (TransitionType) EditorGUILayout.EnumPopup("Transition type", instance.TransitionType);
            instance.TransitionDuration = EditorGUILayout.DelayedFloatField("Transition Duration", instance.TransitionDuration);

            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);

            // Default values
            GUILayout.BeginVertical();
            EditorGUILayout.LabelField("Default values", EditorStyles.boldLabel);
            for (var index = 0; index < instance._defaultValues.Count; index++)
            {
                ADefaultValue defaultValue = instance._defaultValues[index];
                GUILayout.BeginVertical(EditorStyles.helpBox);
                GUILayout.BeginHorizontal(EditorStyles.helpBox);
                EditorGUILayout.LabelField(defaultValue.GetType().Name, EditorStyles.boldLabel);

                if (GUILayout.Button("⇧", EditorStyles.miniButtonLeft, miniButtonWidth))
                {
                    if (index > 0)
                    {
                        var temp = instance._defaultValues[index - 1];
                        instance._defaultValues[index - 1] = defaultValue;
                        instance._defaultValues[index] = temp;
                    }
                }

                if (GUILayout.Button("⇩", EditorStyles.miniButtonLeft, miniButtonWidth))
                {
                    if (index < instance._defaultValues.Count - 1)
                    {
                        var temp = instance._defaultValues[index + 1];
                        instance._defaultValues[index + 1] = defaultValue;
                        instance._defaultValues[index] = temp;
                    }
                }

                if (GUILayout.Button("+", EditorStyles.miniButtonLeft, miniButtonWidth))
                {
                    instance._defaultValues.Insert(index, defaultValue.Clone());
                }

                if (GUILayout.Button("-", EditorStyles.miniButtonLeft, miniButtonWidth))
                {
                    instance._defaultValues.RemoveAt(index);
                }

                GUILayout.EndHorizontal();
                EditorGUI.BeginChangeCheck();
                defaultValue.OnGUI();
                if (EditorGUI.EndChangeCheck())
                    EditorUtility.SetDirty(instance);
                GUILayout.EndVertical();
            }

            GUILayout.EndVertical();

            GUILayout.BeginHorizontal();
            selectedDefaultValueIndex = EditorGUILayout.Popup(new GUIContent("Add new Default value:"),
                selectedDefaultValueIndex, defaultValueTypes.Select(type => type.Name).ToArray());

            if (GUILayout.Button("Add"))
            {
                instance._defaultValues.Add(
                    Activator.CreateInstance(defaultValueTypes[selectedDefaultValueIndex]) as ADefaultValue);
            }

            GUILayout.EndHorizontal();



            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            // Tweens
            GUILayout.BeginVertical();
            EditorGUILayout.LabelField("Tweens", EditorStyles.boldLabel);
            for (var index = 0; index < instance._tweenElements.Count; index++)
            {
                ATweenElement tweenElement = instance._tweenElements[index];
                GUILayout.BeginVertical(EditorStyles.helpBox);
                GUILayout.BeginHorizontal(EditorStyles.helpBox);
                EditorGUILayout.LabelField(tweenElement.GetType().Name, EditorStyles.boldLabel);

                if (GUILayout.Button("⇧", EditorStyles.miniButtonLeft, miniButtonWidth))
                {
                    if (index > 0)
                    {
                        var temp = instance._tweenElements[index - 1];
                        instance._tweenElements[index - 1] = tweenElement;
                        instance._tweenElements[index] = temp;
                    }
                }

                if (GUILayout.Button("⇩", EditorStyles.miniButtonLeft, miniButtonWidth))
                {
                    if (index < instance._tweenElements.Count - 1)
                    {
                        var temp = instance._tweenElements[index + 1];
                        instance._tweenElements[index + 1] = tweenElement;
                        instance._tweenElements[index] = temp;
                    }
                }

                if (GUILayout.Button("+", EditorStyles.miniButtonLeft, miniButtonWidth))
                {
                    instance._tweenElements.Insert(index, tweenElement.Clone());
                }

                if (GUILayout.Button("-", EditorStyles.miniButtonLeft, miniButtonWidth))
                {
                    instance._tweenElements.RemoveAt(index);
                }

                GUILayout.EndHorizontal();
                EditorGUI.BeginChangeCheck();
                tweenElement.OnGUI();
                if (EditorGUI.EndChangeCheck())
                    EditorUtility.SetDirty(instance);
                GUILayout.EndVertical();
            }

            GUILayout.EndVertical();

            GUILayout.BeginHorizontal();
            selectedTweenIndex = EditorGUILayout.Popup(new GUIContent("Add new transition Tween:"),
                selectedTweenIndex, tweenTypes.Select(type => type.Name).ToArray());

            if (GUILayout.Button("Add"))
            {
                instance._tweenElements.Add(Activator.CreateInstance(tweenTypes[selectedTweenIndex]) as ATweenElement);
            }

            GUILayout.EndHorizontal();
        }
    }
#endif
}
