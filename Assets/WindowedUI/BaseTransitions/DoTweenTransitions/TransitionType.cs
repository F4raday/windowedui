﻿namespace WindowedUI.BaseTransitions
{
    public enum TransitionType
    {
        Open = 0,
        Close = 1,
        Custom = 2,
    }
}
