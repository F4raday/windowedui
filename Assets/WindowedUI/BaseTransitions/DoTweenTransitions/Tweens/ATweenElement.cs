﻿using DG.Tweening;
using UnityEngine;
using WindowedUI.Core;

namespace WindowedUI.BaseTransitions
{
    [System.Serializable]
    public abstract class ATweenElement
    {
        public RectTransform InteractedObject;
        public string ObjectName;
        public float StartTime;
        public float Duration;
        public Ease Ease = Ease.OutQuad;

        public virtual void Play(UIWindow window, DoTweenTransition transition, Sequence sequence)
        {
            Play(transition.GetObject(ObjectName), window, transition, sequence);
        }

        public virtual void Play(RectTransform transform, UIWindow window, DoTweenTransition transition,
            Sequence sequence)
        {

        }

        public ATweenElement Clone() => (ATweenElement) MemberwiseClone();

#if UNITY_EDITOR
        internal void OnGUI()
        {
            DrawGUI();
        }

        protected abstract void DrawGUI();
#endif
    }
}
