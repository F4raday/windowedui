﻿using DG.Tweening;
using UnityEditor;
using UnityEngine;
using WindowedUI.Core;

namespace WindowedUI.BaseTransitions
{
    [System.Serializable]
    public class WindowStateBlockerTween : ATweenElement
    {
        public bool IsOpening = true;

        public override void Play(RectTransform transform, UIWindow window, DoTweenTransition transition,
            Sequence sequence)
        {
            sequence.InsertCallback(StartTime,
                () =>
                {
                    if (IsOpening)
                        window.OpenBlockers.Remove(transition);
                    else
                        window.CloseBlockers.Remove(transition);
                });
        }

        public static void SetState(UIWindow window, DoTweenTransition transition, Sequence sequence, bool isOpening, float startTime)
        {
            sequence.InsertCallback(startTime,
                () =>
                {
                    if (isOpening)
                        window.OpenBlockers.Remove(transition);
                    else
                        window.CloseBlockers.Remove(transition);
                });
        }

#if UNITY_EDITOR
        protected override void DrawGUI()
        {
            ObjectName = EditorGUILayout.DelayedTextField("Object name", ObjectName);
            StartTime = EditorGUILayout.FloatField("Start time", StartTime);

            IsOpening = EditorGUILayout.Toggle("Is opening", IsOpening);

            if (string.IsNullOrEmpty(ObjectName))
                EditorGUILayout.HelpBox("Should not be used on main object if Transition type is not Custom", MessageType.Warning);
        }
#endif
    }
}
