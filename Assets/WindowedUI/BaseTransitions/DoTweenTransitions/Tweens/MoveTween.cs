﻿using DG.Tweening;
using UnityEditor;
using UnityEngine;
using WindowedUI.Core;

namespace WindowedUI.BaseTransitions
{
    [System.Serializable]
    public class MoveTween : ATweenElement
    {
        public Vector3 EndPosition;
        public bool IsWorldPosition;

        public override void Play(RectTransform transform, UIWindow window, DoTweenTransition transition,
            Sequence sequence)
        {
            if (IsWorldPosition)
            {
                sequence.Insert(StartTime, transform.DOMove(EndPosition, Duration)
                    .SetEase(Ease));
            }
            else
            {
                sequence.Insert(StartTime, transform.DOAnchorePosition(EndPosition, Duration)
                    .SetEase(Ease));
            }
        }

#if UNITY_EDITOR
        protected override void DrawGUI()
        {
            ObjectName = EditorGUILayout.DelayedTextField("Object name", ObjectName);
            StartTime = EditorGUILayout.FloatField("Start time", StartTime);
            Duration = EditorGUILayout.FloatField("Duration", Duration);
            Ease = (Ease)EditorGUILayout.EnumPopup("Easing", Ease);

            EndPosition = EditorGUILayout.Vector3Field("End position", EndPosition);
            IsWorldPosition = EditorGUILayout.Toggle("Is world position", IsWorldPosition);
        }
#endif
    }
}
