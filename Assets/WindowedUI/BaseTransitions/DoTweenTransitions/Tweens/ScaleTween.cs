﻿using DG.Tweening;
using UnityEditor;
using UnityEngine;
using WindowedUI.Core;

namespace WindowedUI.BaseTransitions
{
    public class ScaleTween : ATweenElement
    {
        public Vector3 EndScale;

        public override void Play(RectTransform transform, UIWindow window, DoTweenTransition transition,
            Sequence sequence)
        {
            sequence.Insert(StartTime, transform.DOScale(EndScale, Duration).SetEase(Ease));
        }

#if UNITY_EDITOR
        protected override void DrawGUI()
        {
            ObjectName = EditorGUILayout.DelayedTextField("Object name", ObjectName);
            StartTime = EditorGUILayout.FloatField("Start time", StartTime);
            Duration = EditorGUILayout.FloatField("Duration", Duration);
            Ease = (Ease)EditorGUILayout.EnumPopup("Easing", Ease);

            EndScale = EditorGUILayout.Vector3Field("End scale", EndScale);
        }
#endif
    }
}
