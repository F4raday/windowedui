﻿using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using UnityEngine;
using UnityEngine.UI;

namespace WindowedUI.BaseTransitions
{
    public static class TweenExtensions
    {
        public static Tween DOAnchorePosition(this RectTransform rectTransform, Vector3 to, float duration)
        {
            Vector3 GetPosition()
            {
                return rectTransform.anchoredPosition3D;
            }

            void SetPosition(Vector3 value)
            {
                rectTransform.anchoredPosition3D = value;
            }

            return DOTween.To(GetPosition, SetPosition, to, duration);
        }

        public static TweenerCore<Color, Color, ColorOptions> DOFade(this Image target, float endValue, float duration)
        {
            TweenerCore<Color, Color, ColorOptions> t = DOTween.ToAlpha(() => target.color, x => target.color = x, endValue, duration);
            t.SetTarget(target);
            return t;
        }

        public static Tween DOFade(this CanvasGroup canvasGroup, float endValue, float duration)
        {
            float GetPosition()
            {
                return canvasGroup.alpha;
            }

            void SetPosition(float value)
            {
                canvasGroup.alpha = value;
            }

            return DOTween.To(GetPosition, SetPosition, endValue, duration);
        }
    }
}
