﻿using DG.Tweening;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;
using WindowedUI.Core;

namespace WindowedUI.BaseTransitions
{
    public class FadeTween : ATweenElement
    {
        public bool IsImage;
        public float EndValue = 1f;

        public override void Play(RectTransform transform, UIWindow window, DoTweenTransition transition,
            Sequence sequence)
        {
            sequence.Insert(StartTime,
                IsImage
                    ? transform.GetComponent<Image>().DOFade(EndValue, Duration).SetEase(Ease)
                    : transform.GetComponent<CanvasGroup>().DOFade(EndValue, Duration).SetEase(Ease));
        }

#if UNITY_EDITOR
        protected override void DrawGUI()
        {
            //InteractedObject = (RectTransform)EditorGUILayout.ObjectField("Interacted object", InteractedObject,
            //    typeof(RectTransform), true);

            IsImage = EditorGUILayout.Toggle("Is image (not CanvasGroup)", IsImage);
            ObjectName = EditorGUILayout.DelayedTextField("Object name", ObjectName);
            StartTime = EditorGUILayout.FloatField("Start time", StartTime);
            Duration = EditorGUILayout.FloatField("Duration", Duration);
            Ease = (Ease)EditorGUILayout.EnumPopup("Easing", Ease);

            EndValue = EditorGUILayout.FloatField("End value", EndValue);
        }
#endif
    }
}
