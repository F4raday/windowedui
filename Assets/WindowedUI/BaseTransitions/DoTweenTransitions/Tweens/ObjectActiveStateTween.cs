﻿using DG.Tweening;
using UnityEditor;
using UnityEngine;
using WindowedUI.Core;

namespace WindowedUI.BaseTransitions
{
    [System.Serializable]
    public class ObjectActiveStateTween : ATweenElement
    {
        public bool NewState;

        public override void Play(RectTransform transform, UIWindow window, DoTweenTransition transition,
            Sequence sequence)
        {
            sequence.InsertCallback(StartTime, () => transform.gameObject.SetActive(NewState));
        }

#if UNITY_EDITOR
        protected override void DrawGUI()
        {
            ObjectName = EditorGUILayout.DelayedTextField("Object name", ObjectName);
            StartTime = EditorGUILayout.FloatField("Start time", StartTime);

            NewState = EditorGUILayout.Toggle("Object state", NewState);

            if (string.IsNullOrEmpty(ObjectName))
                EditorGUILayout.HelpBox("Should not be used on main object", MessageType.Warning);
        }
#endif
    }
}
