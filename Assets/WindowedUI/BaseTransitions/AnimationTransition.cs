﻿using UnityEngine;
using WindowedUI.Core;

namespace WindowedUI.BaseTransitions
{
    public class AnimationTransition : Transition
    {
        [SerializeField] private Animation _animation;
        //[SerializeField] private string clipName;

        public override float Duration => _animation.clip.length;

        public override void Play(UIWindow window)
        {
            _animation.Play();
        }
    }
}
