﻿using UnityEngine;
using WindowedUI.Core;

namespace WindowedUI.Helpers
{
    public class TransitionDebugger : MonoBehaviour
    {
        public UIWindow window;
        public Transition Transition;

        [ContextMenu("Play")]
        public void Play()
        {
            Transition.Play(window);
        }
    }
}
