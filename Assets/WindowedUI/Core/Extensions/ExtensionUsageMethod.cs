﻿namespace WindowedUI.Core.Extensions
{
    [System.Flags]
    public enum ExtensionUsageMethod
    {
        BeforeInit = 1 << 0,
        AfterInit = 1 << 1,
        BeforeOpen = 1 << 2,
        AfterOpen = 1 << 3,
        BeforeClose = 1 << 4,
        AfterClose = 1 << 5,
    }
}
