﻿using UnityEngine;

namespace WindowedUI.Core.Extensions
{
    public abstract class WindowExtension : MonoBehaviour
    {
        [SerializeField] protected ExtensionUsageMethod _extensionUsageMethod;

        public UIWindow ParentWindow { get; private set; }
        protected internal ExtensionUsageMethod ExtensionUsageMethod => _extensionUsageMethod;

        internal void Init(UIWindow window) => ParentWindow = window;
        protected internal virtual void OnBeforeInit() { }
        protected internal virtual void OnAfterInit() { }
        protected internal virtual void OnBeforeWindowOpened() {}
        protected internal virtual void OnAfterWindowOpened() { }
        protected internal virtual void OnBeforeWindowClosed() { }
        protected internal virtual void OnAfterWindowClosed() { }
    }
}
