﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WindowedUI.Core.Extensions;
using WindowedUI.Core.Interfaces;

namespace WindowedUI.Core
{
    public sealed class UIWindow : MonoBehaviour
    {
        [SerializeField] private bool openByDefault;
        [SerializeField] private List<WindowView> views = new List<WindowView>();
        [SerializeField] private List<WindowExtension> extensions = new List<WindowExtension>();
        [SerializeField] private bool manualCallbacks;

        public RootUI Root { get; private set; }
        public WindowState CurrentState { get; private set; }
        public bool OpenByDefault => openByDefault;

        public List<IWindowBlocker> OpenBlockers { get; private set; }
        public List<IWindowBlocker> CloseBlockers { get; private set; }

        internal WindowStagesFlags StagesFlags { get; set; } = WindowStagesFlags.Full;
        internal List<WindowView> Views => views;
        internal List<WindowExtension> Extensions => extensions;
        
        private Coroutine openCoroutine;
        private Coroutine closeCoroutine;

        public TWindowView GetView<TWindowView>() where TWindowView : WindowView
        {
            return views.FirstOrDefault(extension => extension.GetType() == typeof(TWindowView)) as TWindowView;
        }

        public TWindowView V<TWindowView>() where TWindowView : WindowView => GetView<TWindowView>();

        public TWindowExtension GetExtension<TWindowExtension>() where TWindowExtension : WindowExtension
        {
            return extensions.FirstOrDefault(extension => extension.GetType() == typeof(TWindowExtension)) as TWindowExtension;
        }

        public TWindowExtension E<TWindowExtension>() where TWindowExtension : WindowExtension => GetExtension<TWindowExtension>();

        public List<TWindowExtension> GetExtensions<TWindowExtension>() where TWindowExtension : WindowExtension
        {
            return extensions.Where(extension => extension.GetType() == typeof(TWindowExtension)).Cast<TWindowExtension>().ToList();
        }

        public void Open(OpenMethod openMethod = OpenMethod.OpenOnTop)
        {
            StagesFlags = WindowStagesFlags.Full;
            Root.OpenWindow(this, openMethod);
        }

        public void Close()
        {
            Root.CloseWindow(this);
        }

        internal void Init(RootUI uiMain)
        {
            Root = uiMain;

            OpenBlockers = new List<IWindowBlocker>();
            CloseBlockers = new List<IWindowBlocker>();

            foreach (WindowExtension extension in Extensions)
            {
                if (extension != null)
                    extension.Init(this);
            }

            foreach (WindowExtension extension in Extensions)
            {
                if (extension != null && extension.ExtensionUsageMethod.HasFlag(ExtensionUsageMethod.BeforeInit))
                    extension.OnBeforeInit();
            }

            foreach (WindowView windowView in views)
            {
                if (windowView == null)
                {
                    continue;
                }

                windowView.Init(this);
            }

            foreach (WindowExtension extension in Extensions)
            {
                if (extension != null && extension.ExtensionUsageMethod.HasFlag(ExtensionUsageMethod.AfterInit))
                    extension.OnAfterInit();
            }
        }

        internal void PerformOpen()
        {
            gameObject.SetActive(true);

            if (openCoroutine != null)
                StopCoroutine(openCoroutine);

            openCoroutine = StartCoroutine(OpenCoroutine());
        }

        internal void PerformClose()
        {
            if (closeCoroutine != null)
                StopCoroutine(closeCoroutine);

            closeCoroutine = StartCoroutine(CloseCoroutine());
        }

        private IEnumerator OpenCoroutine()
        {
            foreach (WindowExtension extension in Extensions)
            {
                if (extension != null && extension.ExtensionUsageMethod.HasFlag(ExtensionUsageMethod.BeforeOpen))
                    extension.OnBeforeWindowOpened();
            }

            CurrentState = WindowState.Opening;

            if (!manualCallbacks)
            {
                foreach (WindowView view in views)
                {
                    if (view == null)
                    {
                        continue;
                    }

                    view.Open();
                }
            }

            while (OpenBlockers.Count > 0)
                yield return null;

            CurrentState = WindowState.Opened;
            foreach (WindowExtension extension in Extensions)
            {
                if (extension != null && extension.ExtensionUsageMethod.HasFlag(ExtensionUsageMethod.AfterOpen))
                    extension.OnAfterWindowOpened();
            }
        }

        private IEnumerator CloseCoroutine()
        {
            foreach (WindowExtension extension in Extensions)
            {
                if (extension != null && extension.ExtensionUsageMethod.HasFlag(ExtensionUsageMethod.BeforeClose))
                    extension.OnBeforeWindowClosed();
            }

            CurrentState = WindowState.Closing;

            if (!manualCallbacks)
            {
                foreach (WindowView windowView in views)
                {
                    if (windowView == null)
                    {
                        continue;
                    }

                    windowView.Close();
                }
            }

            while (CloseBlockers.Count > 0)
                yield return null;
            
            CurrentState = WindowState.Closed;
            gameObject.SetActive(false);

            foreach (WindowExtension extension in Extensions)
            {
                if (extension != null && extension.ExtensionUsageMethod.HasFlag(ExtensionUsageMethod.AfterClose))
                    extension.OnAfterWindowClosed();
            }
        }
    }
}
