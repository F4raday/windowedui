﻿using UnityEngine;
using WindowedUI.Core.Interfaces;

namespace WindowedUI.Core
{
    public abstract class Transition : ScriptableObject, IWindowBlocker
    {
        public abstract float Duration { get; }
        public virtual RectTransform GetObject(string objectName) => null;
        public abstract void Play(UIWindow window);
    }
}
