﻿namespace WindowedUI.Core
{
    public enum WindowStagesFlags
    {
        Init = 1 << 0,
        Open = 1 << 1,
        Close = 1 << 2,

        Full = Init | Open | Close
    }
}
