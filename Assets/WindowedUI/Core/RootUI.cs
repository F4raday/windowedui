﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using WindowedUI.Core.Extensions;

namespace WindowedUI.Core
{
    public class RootUI : MonoBehaviour
    {
        [SerializeField] private List<UIWindow> windows = new List<UIWindow>();
        [SerializeField] private bool isMain;
        [SerializeField] private bool disableWindowsGameObjects = true;
        [SerializeField] private bool autoInit;
        [SerializeField] private bool validateState;

        public static RootUI MainUI { get; private set; }

        private List<UIWindow> currentOpenedWindows;
        private bool isInitialized;

        private List<WindowExtension> extensions;

        private void Awake()
        {
            if (isMain)
            {
                if (MainUI == null)
                {
                    MainUI = this;
                }
                else
                {
                    Debug.LogError($"Another RootUI is marked as Main. Make sure only one Main UI object is in the project");
                }
            }

            if (disableWindowsGameObjects)
            {
                foreach (UIWindow window in windows)
                {
                    window.gameObject.SetActive(false);
                }
            }

            if (autoInit)
                Init(false);
        }

        private void Start()
        {
            if(autoInit)
                OpenDefaultWindows();
        }

        public void Init(bool openDefaultWindows = true)
        {
            if(isInitialized)
            {
                Debug.LogError("UI already initialized.", this);
                return;
            }

            isInitialized = true;

            InitWindows();

            if(openDefaultWindows)
                OpenDefaultWindows();
        }

        public void OpenWindow(string windowName, OpenMethod openMethod = OpenMethod.OpenOnTop)
        {
            OpenWindow(windows.FirstOrDefault(window => window.name == windowName));
        }

        public void CloseWindow(string windowName)
        {
            CloseWindow(windows.FirstOrDefault(window => window.name == windowName));
        }

        public void OpenWindow(UIWindow uiWindow, OpenMethod openMethod = OpenMethod.OpenOnTop)
        {
            if (uiWindow == null)
            {
                Debug.LogError($"Can't open window. It's null");
                return;
            }

            if (validateState && !uiWindow.CurrentState.CanOpen())
                return;

            switch (openMethod)
            {
                case OpenMethod.CloseAll:
                    for (int i = currentOpenedWindows.Count - 1; i >= 0; i--)
                    {
                        currentOpenedWindows[i].PerformClose();
                        currentOpenedWindows.RemoveAt(i);
                    }
                    break;
                case OpenMethod.ClosePrevious:
                    currentOpenedWindows[currentOpenedWindows.Count - 1].PerformClose();
                    currentOpenedWindows.RemoveAt(currentOpenedWindows.Count - 1);
                    break;
                case OpenMethod.OpenOnTop:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(openMethod), openMethod, null);
            }

            currentOpenedWindows.Add(uiWindow);
            uiWindow.PerformOpen();
        }

        public void CloseWindow(UIWindow window)
        {
            if(!currentOpenedWindows.Contains(window))
                Debug.LogWarning($"Can't properly close window {window.name}. It's not registered opened", window);

            if (validateState && !window.CurrentState.CanClose())
                return;

            currentOpenedWindows.Remove(window);
            window.PerformClose();
        }

        public void CloseAll()
        {
            for (int i = currentOpenedWindows.Count - 1; i >= 0; i--)
            {
                CloseWindow(currentOpenedWindows[i]);
            }
        }

        public UIWindow GetWindow(string windowName)
        {
            return windows.FirstOrDefault(window => window.name == windowName);
        }

        private void InitWindows()
        {
            currentOpenedWindows = new List<UIWindow>();
            extensions = new List<WindowExtension>();
            for (int i = 0; i < windows.Count; i++)
            {
                extensions.AddRange(windows[i].Extensions);
                windows[i].Init(this);
            }
        }

        private void OpenDefaultWindows()
        {
            foreach (UIWindow window in windows)
            {
                if (window.OpenByDefault)
                    OpenWindow(window);
                else
                    window.gameObject.SetActive(false);
            }
        }

        public TWindowExtension GetExtension<TWindowExtension>() where TWindowExtension : WindowExtension
        {
            return extensions.FirstOrDefault(extension => extension.GetType() == typeof(TWindowExtension)) as TWindowExtension;
        }

        public List<TWindowExtension> GetExtensions<TWindowExtension>() where TWindowExtension : WindowExtension
        {
            return extensions.Where(extension => extension.GetType() == typeof(TWindowExtension)).Cast<TWindowExtension>().ToList();
        }
    }
}
