﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;
using WindowedUI.Core.Extensions;


namespace WindowedUI.Core.Editor
{
    [CustomEditor(typeof(UIWindow))]
    public class UIWindowEditor : UnityEditor.Editor
    {
        UIWindow window;

        private void OnEnable()
        {
            window = target as UIWindow;
        }

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            if (GUILayout.Button("Collect views"))
            {
                window.Views.Clear();
                window.Views.AddRange(window.GetComponentsInChildren<WindowView>());

                EditorUtility.SetDirty(window);
            }

            if (GUILayout.Button("Collect extensions"))
            {
                window.Extensions.Clear();
                window.Extensions.AddRange(window.GetComponentsInChildren<WindowExtension>());

                EditorUtility.SetDirty(window);
            }
        }
    }
}
#endif