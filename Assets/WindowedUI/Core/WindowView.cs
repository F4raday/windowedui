﻿using UnityEngine;

namespace WindowedUI.Core
{
    public abstract class WindowView : MonoBehaviour
    {
        public UIWindow ParentWindow { get; private set; }

        internal virtual void Init(UIWindow window)
        {
            ParentWindow = window;
            OnInit();
        }

        internal void Open() => OnOpen();
        internal void Close() => OnClose();

        protected virtual void OnInit() { }

        protected virtual void OnOpen() { }

        protected virtual void OnClose() { }
    }
}
