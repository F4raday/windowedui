﻿using System;

namespace WindowedUI.Core
{
    public enum WindowState
    {
        Missing = 0,
        Opening = 1,
        Opened = 2,
        Closing = 3,
        Closed = 4,
    }

    public static class WindowStateExtension
    {
        public static bool CanOpen(this WindowState currentState)
        {
            switch (currentState)
            {
                case WindowState.Missing:
                case WindowState.Closed:
                    return true;
                case WindowState.Opening:
                case WindowState.Opened:
                case WindowState.Closing:
                    return false;
                default:
                    throw new ArgumentOutOfRangeException(nameof(currentState), currentState, null);
            }
        }

        public static bool CanClose(this WindowState currentState)
        {
            switch (currentState)
            {
                case WindowState.Missing:
                case WindowState.Opened:
                case WindowState.Opening:
                    return true;
                case WindowState.Closed:
                case WindowState.Closing:
                    return false;
                default:
                    throw new ArgumentOutOfRangeException(nameof(currentState), currentState, null);
            }
        }
    }
}
