﻿namespace WindowedUI.Core
{
    public enum OpenMethod
    {
        CloseAll = 0,
        ClosePrevious = 1,
        OpenOnTop = 2,
    }
}
